package Utility;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class UserInterface {
	public UserInterface(AndroidDriver<AndroidElement> driver) {
     	PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	public void Sleep(int X) throws InterruptedException {
		X = X*1000;
		Thread.sleep(X);
	}
	                                  //<<<<<<<<<Login>>>>>>>>>//
	
	//StartScreen
	@AndroidFindBy(id="com.android.packageinstaller:id/permission_allow_button")
	public WebElement ALLOW;
	
	@AndroidFindBy(id="com.android.packageinstaller:id/permission_deny_button")
	public WebElement DENY;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/txt_reg_terms_conditions")
	public WebElement TermsAndConditions;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_start")
	public WebElement START;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tv_center3")
	public WebElement DownloadSthat;
	
	
	//Verify Mobile Number Screen
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/spinner_country_code")
	public WebElement SpCountryCode;
	
	  @AndroidFindBy(id="android:id/text1")     //+965(0),+966(1),+968(2),+971(3),+973(4),+974(5).
	  public List<WebElement> CountryCode;	
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/ed_verify_phno")
	public WebElement MobileNumber;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_ok_phentered")
	public WebElement SEND;

	
	//OTP screen
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/resend")
	public WebElement resendcode;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/ed_code")
	public WebElement EnterOtp;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_verify_code")
	public WebElement Verify;
	
	
	                                //<<<<<<<<<Signup>>>>>>>>>//
	
	//Create New Account Screen
	@AndroidFindBy(id="android:id/button1")
	public WebElement NewAccSignup;
	
	@AndroidFindBy(id="android:id/button2")
	public WebElement NewAccCANCEL;
	
	
	//SignIn Mobile Number Screen
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/ed_reg_name")
	public WebElement RegisterName;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/ed_reg_email")
	public WebElement RegisterEmail;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/ed_reg_iban")
	public WebElement RegisterIban;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_bank_name")
	public WebElement RegisterBankName;
	  
	  @AndroidFindBy(id="android:id/text1")     // 0 to 11
	  public List<WebElement> RegisterSelectBankName;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_city_name")
	public WebElement CityName;
	
      @AndroidFindBy(id="android:id/text1")     // 0 to 6
	  public List<WebElement> SelectCityName;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvTruckName")
	public WebElement TruckName;
	
	  @AndroidFindBy(id="android:id/text1")     // 0-REGULAR, 1-FLATBED, 2-WINCH
	  public List<WebElement> SelectTruckName;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiCbInsurance")
	public WebElement InsuranceYes;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvNationalId")
	public WebElement NationalId;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Camera']")
	  public WebElement Camera;
	  
	    @AndroidFindBy(id="com.sec.android.app.camera:id/okay")
		public WebElement CameraOK;
	    
	      @AndroidFindBy(id="com.sec.android.gallery3d:id/action_done")
		  public WebElement CameraDONE;
	      
	      @AndroidFindBy(id="com.sec.android.gallery3d:id/action_cancel")
		  public WebElement CameraCANCEL;
	    
	    @AndroidFindBy(id="com.sec.android.app.camera:id/retry")
		public WebElement CameraRETRY;
	  
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Files']")
	  public WebElement Files;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvDrivingLicense")
	public WebElement DrivingLicense;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvCarLicense")
	public WebElement CarLicense;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvCartFrontImage")
	public WebElement TruckFrontImage;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tvCarBackImage")
	public WebElement TruckBackImage;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/btn_register")
	public WebElement REGISTER;	
                                     //<<<<<<<<<HomeScreen>>>>>>>>>//
	
	//Menu
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/navigation_home")
	public WebElement Home;
	
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/driver_ratings")
	  public WebElement Reviews;
	
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/switch_button_online")
	  public WebElement Online;
		
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/navigation_orders")
	public WebElement Orders;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Completed']")
	  public WebElement Completed;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Cancelled']")
	  public WebElement Cancelled;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/navigation_wallet")
	public WebElement Wallet;
	
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiEtAmount")
	  public WebElement Amount;
	
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiAddMoneyToWallet")
	  public WebElement AddMoneyToWallet;
	 
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/backButton")
	    public WebElement CreditCardBackButton;
	    
	      @AndroidFindBy(xpath="//android.widget.Button[@text='YES']")
		  public WebElement CreditCardCanelYes;
	      
	      @AndroidFindBy(xpath="//android.widget.Button[@text='NO']")
		  public WebElement CreditCardCanelNo;
	  
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/cardNumberET")
	    public WebElement CreditCardCardNumber;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/expiryDateET")
	    public WebElement CreditCardExpiryDate;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/cvvET")
	    public WebElement CreditCardCvv;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/holderNameET")
	    public WebElement CreditCardCardHolerName;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/rememberMeTB")
	    public WebElement CreditCardRememberMe;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/payBtn")
	    public WebElement CreditCardPAY;
	
	@AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/navigation_account")
	public WebElement MyAccount;
	
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tv_logout")
	  public WebElement Logout;
	  
	    @AndroidFindBy(id="android:id/button1")
	    public WebElement LogoutOK;
	    
	    @AndroidFindBy(id="android:id/button2")
	    public WebElement LogoutCancel;
	    
	  @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/tv_edit")
      public WebElement MyAccountEdit;  
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiEmail")
	    public WebElement MyAccountEditEmail;
	    
	    @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiUpdate")
	    public WebElement MyAccountUPDATE;
	  
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiOtherOptions")
	  public WebElement OtherOptions; 
      
        @AndroidFindBy(className="android.widget.ImageButton")     //BackButton(0)
	    public List<WebElement> BackButton;
      
        @AndroidFindBy(xpath="//android.widget.TextView[@text='English']")
	    public WebElement English;
        
        @AndroidFindBy(xpath="//android.widget.TextView[@text='العربية']")
	    public WebElement Arabic;      
      
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiSmsSwitch")
	    public WebElement SmsNotification; 
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiEmailSwitch")
	    public WebElement EmailNotification; 
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiRateApp")
	    public WebElement RateApp;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiPriceCard")
	  public WebElement PriceCard;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiPayCommission")
	  public WebElement PayCommision;
      
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiBanksLink")
	    public WebElement PayCommisionBanlUrl;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiName")
	    public WebElement PayCommisionName;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiBankName")
	    public WebElement PayCommisionBankName;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiAmount")
	    public WebElement PayCommisionAmount;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiImageUpload")
	    public WebElement PayCommisionUploadReceipt;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiSubmit")
	    public WebElement PayCommisionSUBMIT;
        
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiMissingDocs")
	  public WebElement MissingDocuments;
      
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiNationalId")
	    public WebElement MissingDocumentsNationalId;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiDrivingLicense")
	    public WebElement MissingDocumentsDrivingLicense;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiCarLicense")
	    public WebElement MissingDocumentsCarLicense;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiCartFrontImage")
	    public WebElement MissingDocumentsTruckFrontImage;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiCarBackImage")
	    public WebElement MissingDocumentsTruckBackImage;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiUploadBtn")
	    public WebElement MissingDocumentsUploadDoc;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiCustSupport")
	  public WebElement CustomerSupport;
      
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/attachmentButton")
	    public WebElement CustomerSupportAttachmentBtn;
        
          @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiLlCamera")
	      public WebElement CustomerSupportCamera;
          
          @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiLlCamera")
	      public WebElement CustomerSupportFiles;
          
          @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiLlCamera")
	      public WebElement CustomerSupportAudioMsg;
          
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/messageInput")
	    public WebElement CustomerSupportMsg;
        
        @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/messageSendButton")
	    public WebElement CustomerSupportSendBtn;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiHelpNo")
	  public WebElement CustomerSupportNumber;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiShareApp")
	  public WebElement ShareApp;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiTwitter")
	  public WebElement TwitterLink;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiInstagram")
	  public WebElement InstagramLink;
      
      @AndroidFindBy(id="com.tmmmt.sthatpartners.beta:id/uiWeb")
	  public WebElement SthatUrl;
	   	                                 //<<<<<<<<<Order>>>>>>>>>//
	
}
