package SPAutomation;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.internal.FindsById;
import Utility.PageHelper;
import Utility.UserInterface;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.KeyEvent;

public  class Server extends Main{
	protected AndroidDriver<AndroidElement> driver;
    public Server(AndroidDriver<AndroidElement> driver) {
	this.driver = driver;
	}
	Logger log = Logger.getLogger(Server.class);	
	
	public void StartScreen() throws InterruptedException {
        UserInterface ui = new UserInterface(driver);
        try {ui.Sleep(3);
		try {
       		for (int i = 0; i < 4; i++){
          		 ui.ALLOW.click();
          		 ui.Sleep(1);} }
       		catch (Exception e) {
		 }
			ui.TermsAndConditions.click();
			ui.Sleep(6);
			driver.navigate().back();
			ui.DownloadSthat.click();
			ui.Sleep(4);
			driver.navigate().back();
			ui.Sleep(2);
			ui.START.click();
			ui.Sleep(2);
			driver.navigate().back();
			ui.Sleep(1);
			log.info("StartScreen UI Testing Successful");
		} catch (Exception e) {
			log.error("StartScreen UI Testing Failed");
		}
	}
    public void Signup(int x,String MobileNo) throws Exception {
    	 UserInterface ui = new UserInterface(driver);
       	 try {ui.Sleep(3);
       		 try {
        		for (int i = 0; i < 4; i++){
             		 ui.ALLOW.click();
             		 ui.Sleep(1);} }
          		catch (Exception e) {
   		 }
       	 ui.START.click();
       	 ui.Sleep(3);
       	 ui.SpCountryCode.click();
       	 ui.CountryCode.get(x).click();
       	 ui.Sleep(2);
   	     ui.MobileNumber.sendKeys(MobileNo);
   	     ui.SEND.click();
   	     ui.Sleep(8);
   	     try {
   	 			PageHelper Helper = new PageHelper();
   	 			String OTP = Helper.getOTP(MobileNo);
   	 		    ui.EnterOtp.sendKeys(OTP);	
   	 		    ui.Sleep(4);
   	 		    ui.Verify.click();
   	 		   ui.Sleep(5);
   	 		} 
   	 		catch(Exception e) {
   	 			log.error("OTP Failed");
   	 		}
   	     ui.NewAccSignup.click();
   	     ui.Sleep(3);
   	     ui.RegisterName.sendKeys("Test");
   	     ui.RegisterEmail.sendKeys("Test"+MobileNo+"@gmail.com");
   	     ui.RegisterIban.sendKeys("IBAN1234567890");
   	     ui.RegisterBankName.click();ui.Sleep(2);
   	     ui.RegisterSelectBankName.get(4).click();ui.Sleep(2);
   	     ui.CityName.click();ui.Sleep(2);
   	     ui.SelectCityName.get(3).click();ui.Sleep(2);
   	     ui.InsuranceYes.click();ui.Sleep(1);
   	     ui.TruckName.click();ui.Sleep(2);
   	     ui.SelectTruckName.get(0).click();
   	     ui.InsuranceYes.click();ui.Sleep(1);
   	     ui.NationalId.click();
   	     Camera();
   	     ui.DrivingLicense.click();
   	     Camera();
   	     ui.CarLicense.click();
   	     Camera();
   	     ui.TruckFrontImage.click();
   	     Camera();
   	     ui.TruckBackImage.click();
   	     Camera();
   	     ui.REGISTER.click();ui.Sleep(10);
   	     log.info("Signup Successful");
			
		} catch (Exception e) {
		 log.error("Signup Failed");	
		}
       	 
    }
    @SuppressWarnings("deprecation")
	public void Camera() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	 ui.Camera.click();ui.Sleep(2);
	     driver.pressKeyCode(27);ui.Sleep(4);
	     ui.CameraOK.click();ui.Sleep(2);
	     ui.CameraDONE.click();ui.Sleep(10);
    }
    public void Login(int x,String MobileNo) throws Exception {
    	 UserInterface ui = new UserInterface(driver);
    	 try {ui.Sleep(3);
   	     try {
     		for (int i = 0; i < 4; i++){
        		 ui.ALLOW.click();
        		 ui.Sleep(1);} }
     		catch (Exception e) {
		      }
   	     ui.START.click();
	     ui.Sleep(5);
	     ui.SpCountryCode.click();
	     ui.CountryCode.get(x).click();
	     ui.Sleep(2);
         ui.MobileNumber.sendKeys(MobileNo);
         ui.SEND.click();
         ui.Sleep(5);
 		try {
 			PageHelper Helper = new PageHelper();
 			String OTP = Helper.getOTP(MobileNo);
 		    ui.EnterOtp.sendKeys(OTP);	
 		    ui.Sleep(3);
 		    ui.Verify.click();
 		} 
 		catch(Exception e) {
 			log.error("OTP Failed");
 		}
         log.info("Login Successful");
		} catch (Exception e) {
		 log.error("Login Failed");
		}
   	     
    }
    
    public void Logout() throws Exception {
    	UserInterface ui = new UserInterface(driver);
    	ui.Sleep(3);
    	try {
    		ui.MyAccount.click();
    		ui.Sleep(2);
    		ui.Logout.click();
    		ui.Sleep(1);
    		ui.LogoutOK.click();
    		log.info("Logout Successful");
		} catch (Exception e) {
		    log.error("Logout Failed");
		}  
    }  
    
    public void HomeTab() throws InterruptedException {
 	   UserInterface ui = new UserInterface(driver);
        ui.Sleep(3);
        try {
        	ui.Online.click();
        	ui.Reviews.click();ui.Sleep(2);
        	ui.BackButton.get(0).click();ui.Sleep(1);
        	ui.Online.click();ui.Sleep(1);
            log.info("HomeTab UI Testing Successful");
 		
 	        } catch (Exception e) {
 		    log.error("HomeTab UI Testing Failed");
 	    }       
 	}
   
    public void OrdersTab() throws InterruptedException {
  	   UserInterface ui = new UserInterface(driver);
         ui.Sleep(3);
         try {
         	ui.Orders.click();
         	ui.Cancelled.click();ui.Sleep(2);
         	ui.Completed.click();ui.Sleep(2);
         	ui.Home.click();
             log.info("OrdersTab UI Testing Successful");
  		
  	        } catch (Exception e) {
  		    log.error("OrdersTab UI Testing Failed");
  	    }        
  	}
    
    public void WalletTab() throws InterruptedException {
   	   UserInterface ui = new UserInterface(driver);
          ui.Sleep(3);
          try {
          	ui.Wallet.click();
          	ui.Amount.sendKeys("500");ui.Sleep(2);
          	ui.AddMoneyToWallet.click();ui.Sleep(6);
          	ui.CreditCardCardNumber.sendKeys("5123456789012346");
          	ui.CreditCardExpiryDate.sendKeys("0521");
          	ui.CreditCardCvv.sendKeys("123");
          	ui.CreditCardCardHolerName.sendKeys("Tester");
          	ui.CreditCardRememberMe.click();
          	ui.CreditCardPAY.click();ui.Sleep(15);
          	ui.Home.click();
          	log.info("PAYFORT Testing Successful");
            log.info("WalletTab UI Testing Successful");
   	        } catch (Exception e) {
   		    log.error("WalletTab UI Testing Failed");
   	    }        
   	}
    
    public void MyAccount() throws InterruptedException {
    	   UserInterface ui = new UserInterface(driver);
           ui.Sleep(3);
           MyAccountEdit("x");
           OtherOptions();
           try {
           
           	log.info("WalletTab UI Testing Successful");
    	        } catch (Exception e) {
    		    log.error("WalletTab UI Testing Failed");
    	    }        
    	}
    public void MyAccountEdit(String x) throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {
    		ui.MyAccount.click();ui.Sleep(2);
           	ui.MyAccountEdit.click();
           	ui.MyAccountEditEmail.sendKeys(x);
           	ui.MyAccountUPDATE.click();ui.Sleep(3);
           	ui.Home.click();
           	log.info("MyAccount-Edit UI Testing Successful");
			
		} catch (Exception e) {
			log.error("MyAccount-Edit UI Testing Failed");
		}
    	
    }
    public void OtherOptions() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {ui.MyAccount.click();
    	ui.OtherOptions.click();
    	ui.SmsNotification.click();
    	ui.EmailNotification.click();
    	ui.RateApp.click();ui.Sleep(3);
    	driver.navigate().back();
    	ui.Arabic.click();ui.Sleep(6);
    	ui.MyAccount.click();ui.Sleep(2);
    	ui.OtherOptions.click();
    	ui.English.click();ui.Sleep(6);
      	log.info("Other Options UI Testing Successful");
		
    		} catch (Exception e) {
    			log.error("Other Options UI Testing Failed");
    		}
    	
    }
    public void PriceCard() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {ui.MyAccount.click();
    	ui.PriceCard.click();
    	ui.Sleep(3);
    	ui.BackButton.get(0).click();
    	ui.Sleep(2);
       	ui.Home.click();
       	log.info("PriceCard UI Testing Successful");
		
		} catch (Exception e) {
			log.error("PriceCard UI Testing Failed");
		}
    	
    }
    public void PayCommission() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {ui.MyAccount.click();
    	ui.PayCommision.click();
    	ui.Sleep(1);
    	ui.PayCommisionBanlUrl.click();ui.Sleep(7);
    	driver.navigate().back();
    	ui.PayCommisionName.sendKeys("MyTest");
    	ui.PayCommisionAmount.sendKeys("100");
    	ui.PayCommisionBankName.click();
    	ui.RegisterSelectBankName.get(2).click();
    	ui.Sleep(2);
    	ui.PayCommisionUploadReceipt.click();
    	Camera();ui.Sleep(2);
    	ui.PayCommisionSUBMIT.click();ui.Sleep(3);
       	ui.Home.click();
        log.info("PayCommission UI Testing Successful");
		
		} catch (Exception e) {
			log.error("PayCommission UI Testing Failed");
		}
    	
    }
    public void MissingDocuments() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {
    	ui.MyAccount.click();
    	ui.MissingDocuments.click();
    	ui.MissingDocumentsNationalId.click();
    	Camera();
    	ui.MissingDocumentsDrivingLicense.click();
    	Camera();
    	ui.MissingDocumentsCarLicense.click();
    	Camera();
    	ui.MissingDocumentsTruckFrontImage.click();
    	Camera();
    	ui.MissingDocumentsTruckBackImage.click();
    	Camera();
    	ui.MissingDocumentsUploadDoc.click();
    	ui.Sleep(10);
       	ui.Home.click();
        log.info("MissingDocuments UI Testing Successful");
		
      		} catch (Exception e) {
      			log.error("MissingDocuments UI Testing Failed");
      		}
    }
    public void CustomerSupport() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {
    		ui.MyAccount.click();ui.Sleep(3);
    		ui.CustomerSupportMsg.sendKeys("Hi, This is Test Message");
    	    ui.CustomerSupportSendBtn.click();
    	    ui.CustomerSupportAttachmentBtn.click();
    	    Camera();
    	    ui.BackButton.get(0).click();
    	    ui.Sleep(3);
         	ui.Home.click();
            log.info("CustomerSupport UI Testing Successful");
		
      		} catch (Exception e) {
      			log.error("CustomerSupport UI Testing Failed");
      		}
    }
    public void CustomerSupportNumber() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {
    		ui.MyAccount.click();ui.Sleep(3);
    		ui.CustomerSupportNumber.click();
    		ui.Sleep(5);
    		driver.navigate().back();
         	ui.Home.click();
            log.info("CustomerSupportNumber UI Testing Successful");
		
      		} catch (Exception e) {
      			log.error("CustomerSupportNumber UI Testing Failed");
      		}
    }
    public void ShareApp() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	try {
    		ui.MyAccount.click();ui.Sleep(3);
    		ui.ShareApp.click();
    		ui.Sleep(3);
    		driver.navigate().back();
         	ui.Home.click();
            log.info("ShareApp UI Testing Successful");
		
      		} catch (Exception e) {
      			log.error("ShareApp UI Testing Failed");
      		}
    }
    public void ConnectSthat() {
    	UserInterface ui = new UserInterface(driver);
    	try {
    		ui.MyAccount.click();
    		ui.TwitterLink.click();ui.Sleep(5);driver.navigate().back();
    		ui.InstagramLink.click();ui.Sleep(5);driver.navigate().back();
    		ui.SthatUrl.click();ui.Sleep(5);driver.navigate().back();
    		log.info("Twitter, Instagram & SthatUrl Links UI Testing Successful");
		} catch (Exception e) {
			log.error("Twitter, Instagram & SthatUrl Links UI Testing Failed");
		}
    	
    }
}
    
    
  